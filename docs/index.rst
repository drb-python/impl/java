===================
Data Request Broker
===================
---------------------------------
Java Bridge driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-java/month
    :target: https://pepy.tech/project/drb-driver-java
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-java.svg
    :target: https://pypi.org/project/drb-driver-java/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-java.svg
    :target: https://pypi.org/project/drb-driver-java/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-java.svg
    :target: https://pypi.org/project/drb-driver-java/
    :alt: Python Version Support Badge


-------------------

This python module includes data model implementation of Java Bridge

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/introduction
   user/install
   user/quickstarts


.. toctree::
   :maxdepth: 2

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

Others
======
.. toctree::
   :maxdepth: 2

   user/troubleshooting
   user/limitation