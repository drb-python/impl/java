
import os

_PATH_TEST = "/home/debian/repo_git_impl_java/tests/files/"

os.environ['CLASSPATH_ADDON'] = _PATH_TEST + "*"

from drb.drivers.java.drb_driver_java_factory import DrbJavaFactory

import drb.topics.resolver


path_file_sdf = _PATH_TEST + "ceos.dat"
path_file_sdf = _PATH_TEST + "ceos.dat"


path_file_sdf = _PATH_TEST + "ceos.dat"
item_class, node_file = drb.topics.resolver.resolve(path_file_sdf)
node_java = DrbJavaFactory().create(node_file)
node_sdf = node_java[0]
if node_sdf is not None:

    for index_child in range(len(node_sdf)):
        item = node_sdf[index_child]
        print('child ' + str(index_child) + ':' + item.name + ':'
              + str(item.value))

    recordSequenceNumber = node_sdf['recordSequenceNumber', 'http://www.gael.fr/schemas/ceos'].value

    print("recordSequenceNumber:" + str(recordSequenceNumber))

    node_sdf.close()
