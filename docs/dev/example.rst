.. _example:

Example
=======

Navigate in a SDF java resolved node
-----------------------

.. literalinclude:: example/navigate.py
    :language: python
