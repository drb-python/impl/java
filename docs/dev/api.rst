.. _api:

Reference API
=============

DrbJavaNode
-------
.. autoclass:: drb.drivers.java.drb_impl_java_node.DrbJavaNode
   :members:
   :inherited-members: has_impl,get_impl
   :special-members: get_impl, has_impl, value, children, close
   :private-members: _get_named_child,

DrbJavaBaseNode
-------
.. autoclass:: drb.drivers.java.drb_impl_java_node.DrbJavaBaseNode
   :members:
   :inherited-members: get_impl, has_impl, value, children, close
   :special-members: node_java
