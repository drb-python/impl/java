.. Troubleshooting:
Troubleshooting
=====
Installation
-----------

If you encounter error on downloading jar when pip install drb-driver-java

Check that you have access to repository
https://repository.gael-systems.com/repository/public/

To configure maven repository in ini file refer to jip documentation
https://pypi.org/project/jip/

'Unable to find libjvm.so' error
-----------
When load library if you encounter error like this
Check you environment variable JAVA_HOME
If the problem persists try with another JVM

'NoClassDefFoundError' error
-----------

When load library if you encounter error like:
``jnius.JavaException: JVM exception occurred: fr/gael/drb/value/String java.lang.NoClassDefFoundError``

Check that ``javalib`` directory is present on your env python
Check that jars are present in this directory

If your jar are installed on another directory set CLASSPATH to this directory

.. code-block::

    export CLASSPATH=/mypath_java/*

If not retry install of library, check that jar are correctly downloaded

SDF format not recognized
-----------

Check that you correctly defined CLASSPATH_ADDON

.. code-block::

    export CLASSPATH=/mypath_java/*

or in python code before load drb-driver-java module

.. code-block::

    os.environ['CLASSPATH_ADDON'] = '/mypath_java/*'
    ...
    from drb.factory import DrbFactoryResolver  # noqa: E402

Check that the directory where the schema is present is set in CLASSPATH (before load library)
The path of the schema or addon must be after the path of javalib and separate by ':'  in the string


.. code-block::

   export CLASSPATH=/mypath/venv/javalib/*:/mypath/path_of_addon/*

is correct the add on will be recognize

But in this order

.. code-block::

   export CLASSPATH=/mypath/path_of_addon/*:/mypath/venv/javalib/*

The addon will be not recognize



