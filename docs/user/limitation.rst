.. _limitation:

Limitation
============
General
-----------
The current version does not manage child modification and insertion. JavaNode is currently read only.
Currently, DRB project is not tested on Windows environment and some issues may
appear on Windows systems. If any troubleshoot occurred with the DRB library,
please report us, opening a new accident on the project GitLab page.

Conversion values and attributes from java to python
-----------
Currently, this Java bridge library convert type Date to epoch in milliseconds from January 1st, 1970 at 00:00:00 UTC
For duration we used the default DRB value conversion function that correspond  to seconds, except for YearMonthDuration that are converted in number of month

The binary type value is not supported and concerted if possible in String by java.

For numeric value they are converted to similar type in python, idem for string and byte.

GetImpl
-----------
We simulate a BytesIO implementation by used the implementation of InputStream from java drb node
But for seek we use the position and skip if possible, depend of implementation of InputStream.
