.. _introduction:

What is DRB Driver Java bridge?
#######################
The drb-driver-java python module aims to wrap Java version of DRB model.
Java DRB Node is the first implementation of the model representing data with a node hierarchy.
It supports up to 2 hundred of formats (https://www.gael-systems.com/products/data-request-broker).
The java version implements a set of interesting features such as XQuery/XPath engine, SDF (Structured Data Format), and the cortex model based on OWL ontologies to share applicative properties in its addons.