.. _quickstart:

Use a SDF model
=====
SDF Schema model
---------
To create you XML schema and ontology associated, refer to drb java documentation Otherwise to use a predefined schema
used previously in DRB java, the files (schema and ontology) must be defined in CLASSPATH_ADDON.
Either you copy it on the javalib directory of python env, otherwise you can add path in CLASSPATH_ADDON env variable:
you can add additional paths by separating it with ':' char

.. code-block::

   export CLASSPATH_ADDON=/the_path_of_schema/*
   export CLASSPATH_ADDON=$CLASSPATH:/other_path_for_other_schema/*

Or in python before load drb modules

.. code-block::

    os.environ['CLASSPATH_ADDON'] = "./Documents/tests/sdf/*"
    from drb.factory import DrbFactoryResolver

Resolve directly your node
---------
You can parse your sdf node like any other drb node
You can create your Drb Java Code, to check for example that the ontology is correct

In python

.. code-block::

    node_sdf = DrbFactoryResolver().create(path_file_sdf)
