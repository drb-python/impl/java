.. _install:

Installation
============

Java have to be installed on system
Java can be installed by command lines

.. code-block::

   apt-get update
   apt-get install default-jdk -y

and environment variable JAVA_HOME must be set

.. code-block::

   echo $JAVA_HOME

if not set JAVA_HOME, can be set by command line similar to below

.. code-block::

   export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/

First install ``jip`` for the java dependencies

.. code-block::

   pip install jip

Finally install drb-driver-java library by command line

.. code-block::

   pip install drb-driver-java
   pip install drb-driver-java

Installation requires python 3.8 or above.


