package fr.gael.drb.impl.interface_python;

public class PythonByteList extends Object
{
   protected byte[] dest;
   protected int sizeBuffer=0;
   protected int offset=0;



   public PythonByteList(byte[] a, int offset, int size)
   {
      this.dest=a;
      this.sizeBuffer = size;
      this.offset = offset;

   }

   public void append_elements(byte[] src, int size_to_copy)
   {
      if (( this.offset + size_to_copy ) <= this.sizeBuffer)
      {
         System.arraycopy(src, 0, this.dest, this.offset, size_to_copy);
         this.offset += size_to_copy;
      }
   }

   public byte[] elements() {
      return this.dest;
   }

   public int sizeBuffer()
   {
      return this.sizeBuffer;
   }

   public int capacity()
   {
      return this.dest.length;
   }

}
