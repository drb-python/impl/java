/*
 * Data Request Broker - DRB API
 * Copyright (C) 2001,2002,2003,2004,2005,2006,2007,2008,2009 GAEL Consultant
 * GNU Lesser General Public License (LGPL)
 * 
 * This file is part of Data Request Broker
 * 
 * Data Request Broker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Data Request Broker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.gael.drb.impl.interface_python;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import fr.gael.drb.impl.URLNode;
import org.apache.log4j.Logger;

/**
 * DrbNode implementation. This interface allows to perform "implementation
 * specific" operations.
 */
public class DrbNodeImpInputStream extends URLNode
{
   static private final Logger logger = Logger.getLogger(DrbNodeImpInputStream.class);


   InputStream input_stream_from_python;

   /**
    * Tests if a specific interface can be provided. This operations tests with
    * a minimum of time and memory consumption if the current implementation can
    * provide a specific interface. It important to consider that hasImpl()
    * provides information about the ability to provide such interface in
    * general cases but not focused on the current instance. It may therefore be
    * impossible to get a specific implementation from a node whereas hasImpl()
    * operation returns true.
    * 
    * @param api The class type to test.
    * @return true if an implementation of the interface can be provided and
    *         false otherwise.
    */
   public boolean hasImpl(final Class api)
   {
      if (api == null)
      {
         return false;
      }


      if (api.isAssignableFrom(InputStream.class))
      {
         return true;
      }
      else
      {
         return super.hasImpl(api);
      }
   }

   /**
    * Returns a specific implementation. This operation returns a reference to
    * an object implementing a specific interface. This mean is useful to
    * benefit by a specific and direct API instead of using the DrbNode
    * interface. The provided object shall represent the current node such as it
    * is possible after a call to this operation, to dismiss the reference to
    * the node. For instance, a file is usually wrapped as a drb.imple.file.File
    * class and thanks to this operation can be seen as a InputStream anabling
    * direct access to the handled file.
    * 
    * @param api The interface (or class) to be provided.
    * @return A reference to the object implementing the interface or a null
    *         reference otherwise.
    */
   @Override
   public Object getImpl(final Class api)
   {

      if (api == null)
      {
         return null;
      }
      if (api.isAssignableFrom(InputStream.class))
      {
         logger.info("get input stream :" + input_stream_from_python.hashCode());

         try
         {
            input_stream_from_python.reset();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
         return input_stream_from_python;
      }
      return super.getImpl(api);
   }

//   public DrbNodeImpInputStream(final URL url, final InputStream input_stream)
   public DrbNodeImpInputStream(final URL url, final StreamPythonInterface input_stream)
   {
      super(url);

      /**
       * Checks the InputStream.
       */
      if (input_stream == null)
      {
         throw new NullPointerException();
      }

      //this.input_stream_from_python = input_stream;
      this.input_stream_from_python = new StreamPython(input_stream);

      logger.info("set input stream :" +  input_stream_from_python.hashCode());

   }

} // DrbNodeImpl
