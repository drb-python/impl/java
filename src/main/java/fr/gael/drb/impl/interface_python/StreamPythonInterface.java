package fr.gael.drb.impl.interface_python;

public interface StreamPythonInterface
{
   public void reset ();

   public long skip(long pos);

   public int read();

   public int readByte();

  public int readBuffer(PythonByteList  python_buf, int len);

}
