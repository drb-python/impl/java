package fr.gael.drb.impl.interface_python;

import java.util.ArrayList;
import java.util.List;

public class ListDrbNodeImpInputStream
{
   static List<DrbNodeImpInputStream> listPythonNode = new ArrayList();

   static void referenceNode(DrbNodeImpInputStream node)
   {
      listPythonNode.add(node);

   }

   static void unReferenceNode(DrbNodeImpInputStream node)
   {
      listPythonNode.remove(node);
   }
}
