package fr.gael.drb.impl.interface_python;

import java.io.IOException;
import java.io.InputStream;

public class StreamPython extends InputStream
{

   StreamPythonInterface stream_from_python;

   public StreamPython(StreamPythonInterface stream_from_python)
   {
      this.stream_from_python = stream_from_python;

   }


   @Override
   public int read() throws IOException
   {
      int read = this.stream_from_python.readByte();
      if (read < 0)
         return read;
      return read & 255;
   }

   public int read_one() throws IOException
   {
      return stream_from_python.read();
   }


   @Override
   public int read(byte[] b, int off, int len) throws IOException {

      PythonByteList buffer_read = new PythonByteList(b, off, len);
      int read = this.stream_from_python.readBuffer(buffer_read, len);

      return read;
   }

   @Override
   public void reset() throws IOException
   {
      stream_from_python.reset();
   }


   @Override
   public long skip(long pos) throws IOException
   {
      return stream_from_python.skip(pos);
   }
}
